# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.find_or_initialize_by( { first_name: "admin", email: 'admin@gmail.com', role: "admin" } )
user1.attributes = { password: '123456'}
user1.save if user1.new_record?

user2 = User.find_or_initialize_by( { first_name: "customer" ,email: 'customer@gmail.com', role: "customer" } )
user2.attributes = { password: '123456'}
user2.save if user2.new_record?