class CreateDeals < ActiveRecord::Migration[5.2]
  def change
    create_table :deals do |t|
      t.string :title
      t.string :description
      t.decimal :price, precision: 10, scale: 5
      t.decimal :discounted_price, precision: 10, scale: 5
      t.integer :quantity
      t.datetime :publish_date
      t.boolean :is_published
      t.timestamps
    end
  end
end
