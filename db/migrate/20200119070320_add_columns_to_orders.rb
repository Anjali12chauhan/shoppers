class AddColumnsToOrders < ActiveRecord::Migration[5.2]
  def up
    add_column :orders, :actual_price, :decimal, precision: 10, scale: 5
    add_column :orders, :shipping_address, :string
  end

  def down
    remove_column :orders, :actual_price, :decimal, precision: 10, scale: 5
    remove_column :orders, :shipping_address, :string
  end
end
