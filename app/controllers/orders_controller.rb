class OrdersController < ApplicationController

  before_action :set_latest_deal, only: [:new, :create]
  def index
  end

  def new
    @discount_price = @latest_deal.calculate_discount(1)
    @order = Order.new
  end

  def create
    @order = Order.new(order_params)
    @order.user_id = 1
    @order.deal_id = @latest_deal.id
    @order.save
    @latest_deal.decrement!("quantity")
  end

  private

  def order_params
    params.require(:order).permit(:shipping_address, :actual_price)
  end

  def set_latest_deal
    @latest_deal = Deal.where(is_published: true).order("publish_date").last
  end

end
