class SessionsController < ApplicationController
  def new
   @session = Session.new
  end

  def create
    @user = User.find_by(email: params[:email])
    if @user.present? && @user.authenticate(params[:password])
      session[:user_id] = @user.id
      if @user.role == 'admin'
        redirect_to deals_path
      else
        redirect_to publish_deals_path
      end
    else
      flash.now[:error] = "Incorrect Username/Password"
      redirect_to login_path
    end
  end

  def destroy
    session.clear
    redirect_to login_path
  end
end
