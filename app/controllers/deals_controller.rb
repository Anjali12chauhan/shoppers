class DealsController < ApplicationController

  before_action :is_admin?, only: [:index, :new, :create]
  def index
    if is_admin?
      @deals = Deal.all 
    else
      flash.now[:error] = "Customer cannot access it"
      redirect_to login_path
    end
  end

  def new 
    @deal = Deal.new if is_admin?
  end

  def create
    if is_admin?
      @deal = Deal.new(deal_params)
      @deal.save
      @deals = Deal.all
      render 'index'
    end
  end

  def publish
    @deal_to_publish = Deal.where(is_published: true).order("publish_date").last
    @purchasable = @deal_to_publish.valid_to_purchase?(1)
  end

  private 

  def deal_params
    params.require(:deal).permit(:title, :description, :price, :discounted_price, :quantity, :image)
  end

  def is_admin?
    User.find_by_id(session[:user_id]).role == 'admin'
  end
end
