class Deal < ApplicationRecord
  has_many :orders

  has_attached_file :image,
  Shoppers::Configuration.paperclip_options[:deals][:image]
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def publishable?
    title.present? && 
      description.present? && 
        price.present? && 
          discounted_price.present? && 
            quantity.present? && 
              image.present?
  end

  def quantity_left?
    quantity > 0
  end

  def already_purchased?(uid)
    orders.where(user_id: uid).present?
  end

  def valid_to_purchase?(uid)
    quantity_left? && !already_purchased?(uid)
  end

  def calculate_discount(uid)
    user_purchase_count = Order.where(user_id: uid).length
    discount_applied = user_purchase_count > 5 ? discounted_price * 0.05 : discounted_price * ((user_purchase_count)/100.0)
    selling_price = discounted_price - discount_applied 
  end

  def self.publish
    Deal.where(is_published: [nil, false]).each do |unpublished_deal|
      if unpublished_deal.publishable?
        unpublished_deal.update_attributes(is_published: true, publish_date: Time.now)
        break
      end
    end
  end

end
