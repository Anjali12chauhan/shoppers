class User < ApplicationRecord
  has_many :orders
  

  def authenticate(password)
    self.password == password
  end
end
